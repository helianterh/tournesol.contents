﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Runtime.CompilerServices;

namespace Tournesol.Extension.EFCore
{
    public static class Extensions
    {
        public static void UpdateDisconnectedEntities(this DbContext context, IEnumerable<object> entities)
        {
            _UpdateDisconnectedEntities(context, entities, new HashSet<object>());
        }

        private static void _UpdateDisconnectedEntities(this DbContext context, IEnumerable<object> entities, HashSet<object> ignored)
        {
            foreach (var entity in entities)
            {
                if (ignored.Contains(entity))
                    continue;

                ignored.Add(entity);

                var key = context.FindPrimaryKeyValue(entity).ToArray();
                var entityPersistent = context.Find(entity.GetType(), key);

                EntityEntry entry;

                if (entityPersistent != null)
                {
                    entry = context.Entry(entityPersistent);

                    foreach (var navigation in entry.Navigations)
                    {
                        var src = navigation.Metadata.FieldInfo.GetValue(entity);
                        if (src != null)
                            navigation.Load();
                    }

                    entry.CurrentValues.SetValues(entity);

                    foreach (var collection in entry.Collections.ToList())
                    {
                        var src = collection.Metadata.FieldInfo.GetValue(entity);
                        collection.Metadata.FieldInfo.SetValue(entityPersistent, src);
                    }
                }
                else
                    entry = context.Add(entity);

                foreach (var navigation in entry.Navigations)
                {
                    var src = navigation.Metadata.FieldInfo.GetValue(entity);
                    if (src is IEnumerable<object>)
                        _UpdateDisconnectedEntities(context, src as IEnumerable<object>, ignored);
                    else if (src != null)
                        _UpdateDisconnectedEntities(context, src.Yield(), ignored);
                }

            }
        }

        public static IEnumerable<object> FindPrimaryKeyValue<T>(this DbContext dbContext, T entity)
        {
            return from p in dbContext.FindPrimaryKeyProperties(entity)
                   select entity.GetPropertyValue(p.Name);
        }

        public static IReadOnlyList<IProperty> FindPrimaryKeyProperties<T>(this DbContext dbContext, T entity)
        {
            return dbContext.Model.FindEntityType(entity.GetType()).FindPrimaryKey().Properties;
        }

        public static object GetPropertyValue<T>(this T entity, string name)
        {
            return entity.GetType().GetProperty(name).GetValue(entity, null);
        }
    }
}