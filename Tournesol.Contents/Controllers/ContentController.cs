﻿using Microsoft.AspNetCore.Mvc;
using Tournesol.Contents.Common;
using Tournesol.Contents.Common.Model;

namespace Tournesol.Contents.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContentController : ControllerBase
    {
        private ContentRepository _contentRepository;

        public ContentController(ContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        [Route("/api/v1/GetContents")]
        [HttpGet]
        public IEnumerable<Common.Model.Content> GetContents(int contentTypeId)
        {
            return _contentRepository.GetContents((ContentType)contentTypeId);
        }

        [Route("/api/v1/GetContent")]
        [HttpGet]
        public Common.Model.Content? GetContent(Guid contentId)
        {
            return _contentRepository.GetContent(contentId);
        }

        [Route("/api/v1/AddContent")]
        [HttpGet]
        public Common.Model.Content AddContent(int contentTypeId, Guid? parentId, string json)
        {
            return _contentRepository.AddContent((ContentType)contentTypeId, parentId, json);
        }

        [Route("/api/v1/DeleteContent")]
        [HttpPost]
        public void DeleteContent(Guid contentId)
        {
            _contentRepository.DeleteContent(contentId);
        }

        [Route("/api/v1/UpdateContent")]
        [HttpPost]
        public Common.Model.Content UpdateContent(Guid contentId, string json)
        {
            return _contentRepository.UpdateContent(contentId, json);
        }

        [Route("/api/v1/PublishContent")]
        [HttpPost]
        public void PublishContent(Guid contentId)
        {
            _contentRepository.PublishContent(contentId);
        }
    }
}