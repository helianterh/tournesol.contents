using Tournesol.Contents.Common.Interface;
using Tournesol.Contents.Persistence;
using Microsoft.EntityFrameworkCore;
using Tournesol.Contents.Common;
using Tournesol.Content;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(opt => opt.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers();

builder.Services.AddScoped(provider => 
    new ContentRepository(
        new FacadePersistanceEFCore(b =>
        {
            b.UseSqlite("DataSource=Content.db");
        },
        (c,firstTime) =>
        {
            if (firstTime)
            {
                c.Database.EnsureDeleted();
                c.Database.EnsureCreated();
            }
        }),
        new FacadeUser()
    )
);

var app = builder.Build();

app.UseCors(builder => builder.WithOrigins("https://localhost:44475")
                                .AllowAnyMethod()
                                .AllowAnyHeader());

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();


app.Run();
