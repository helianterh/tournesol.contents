﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Attributes
{
    public static class Attributes
    {
        public class KeyValueAttribute : Attribute
        {
            public string KeyValue { get; set; }

            public KeyValueAttribute(string keyValue)
            {
                KeyValue = keyValue;
            }
        }

    }
}
