﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Contents.Common.Interface;
using Tournesol.Contents.Common.Model;
using Tournesol.Extension;

namespace Tournesol.Contents.Common
{
    public class ContentRepository
    {
        private static string CURRENT_API = "1.0.0";

        private IFacadePersistance _facadePersistance;
        private IFacadeUser _facadeUser;
        

        public ContentRepository(IFacadePersistance facadePersistance, IFacadeUser facadeUser)
        {
            _facadePersistance = facadePersistance;
            _facadeUser = facadeUser;
        }

        public Content AddContent(ContentType contentType, Guid? parentId, IContentObject obj)
        {
            return AddContent(contentType, parentId, obj.ToJson());
        }

        public Content AddContent(ContentType contentType, Guid? parentId, string json) 
        {
            var now = DateTime.UtcNow;
            var by = _facadeUser.GetCurrentUserGuid();
            var content = new Content(null, parentId, contentType, json, CURRENT_API, VersionState.Added, by, now, by, now);
            var childObj = Content.CreateObject(contentType);

            //Validate if the parent is valid
            if (parentId.HasValue)
            {
                var parent = _facadePersistance.GetContent(parentId.Value);
                if (parent == null)
                    throw new TournesolException($"The content Id \"{parentId}\" is not known.");

                var parentObj = Content.CreateObject(parent.ContentType);
                
                if (!parentObj.CanHaveChildrenOf(contentType))
                    new TournesolException($"The content of type \"{parent.ContentType}\" can not have children of type \"{contentType}\".");

                if (!childObj.CanBeChildOf(parent.ContentType))
                    new TournesolException($"The content of type \"{contentType}\" can not have a parent of type \"{parent.ContentType}\".");
            }
            else
            {
                if (!childObj.CanBeChildOf(ContentType.None))
                    throw new TournesolException($"The content of type \"{contentType}\" must have a parent.");
            }

            //Create a new version
            content.Versions = new List<ContentVersion>() { new ContentVersion(content) };

            _facadePersistance.UpdateContent(content);
            return _facadePersistance.GetContent(content.ContentId.Value);
        }

        public Content UpdateContent(Guid contentId, string json)
        {
            var content = _facadePersistance.GetContent(contentId);
            if (content == null)
                throw new TournesolException($"The content Id {contentId} is not known.");

            return UpdateContent(content, json);
        }

        public Content UpdateContent(Content content, IContentObject obj)
        {
            return UpdateContent(content, obj.ToJson());
        }

        public Content UpdateContent(Content content, string json)
        {
            var now = DateTime.UtcNow;
            var by = _facadeUser.GetCurrentUserGuid();

            content.Json = json;
            content.Api = CURRENT_API;
            content.UpdatedBy = by;
            content.UpdatedDate = now;

            //Créer une nouvelle version
            if (content.Versions == null) content.Versions = new List<ContentVersion>();
            content.Versions.Add(new ContentVersion(content));

            _facadePersistance.UpdateContent(content);
            return content;
        }

        public void DeleteContent(Guid contentId)
        {
            var content = _facadePersistance.GetContent(contentId);
            if (content == null)
                throw new TournesolException($"The content Id {contentId} is not known.");

            DeleteContent(content);
        }

        public void DeleteContent(Content content)
        {
            var now = DateTime.UtcNow;
            var by = _facadeUser.GetCurrentUserGuid();

            content.State = VersionState.Deleted;
            content.UpdatedBy = by;
            content.UpdatedDate = now;

            //Créer une nouvelle version
            if (content.Versions == null) content.Versions = new List<ContentVersion>();
            content.Versions.Add(new ContentVersion(content));

            _facadePersistance.UpdateContent(content);
        }

        public void PublishContent(Guid contentId)
        {
            var content = _facadePersistance.GetContent(contentId);
            if (content == null)
                throw new TournesolException($"The content Id {contentId} is not known.");

            PublishContent(content);
        }

        public void PublishContent(Content content)
        {
            var now = DateTime.UtcNow;
            var by = _facadeUser.GetCurrentUserGuid();

            content.State = VersionState.Published;
            content.UpdatedBy = by;
            content.UpdatedDate = now;

            //Créer une nouvelle version
            if (content.Versions == null) content.Versions = new List<ContentVersion>();
            content.Versions.Add(new ContentVersion(content));

            _facadePersistance.UpdateContent(content);
        }

        public Content? GetContent(Guid contentId) 
        {
            return _facadePersistance.GetContent(contentId);
        }

        public IEnumerable<Content> GetContents(ContentType contentType)
        {
            return _facadePersistance.GetContents(contentType);
        }
    }
}
