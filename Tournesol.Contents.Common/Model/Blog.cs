﻿using System.ComponentModel.DataAnnotations.Schema;
using Tournesol.Contents.Common.Interface;

namespace Tournesol.Contents.Common.Model
{
    public class Blog : IContentObject
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public bool CanBeChildOf(ContentType contentType)
        {
            return contentType == ContentType.None;
        }

        public bool CanHaveChildrenOf(ContentType contentType)
        {
            return contentType == ContentType.BlogPost ||
                contentType == ContentType.Like ||
                contentType == ContentType.Comment;
        }
    }
}