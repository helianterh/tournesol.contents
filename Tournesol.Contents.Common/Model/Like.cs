﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Contents.Common.Model
{
    public class Like : IContentObject
    {
        public bool IsLike { get; set; }

        public bool CanBeChildOf(ContentType contentType)
        {
            return contentType == ContentType.Blog ||
                contentType == ContentType.BlogPost ||
                contentType == ContentType.Comment;
        }

        public bool CanHaveChildrenOf(ContentType contentType) => false;
    }
}
