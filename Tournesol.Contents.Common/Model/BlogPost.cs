﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using Tournesol.Contents.Common.Interface;

namespace Tournesol.Contents.Common.Model
{
    public class BlogPost : IContentObject
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public bool CanBeChildOf(ContentType contentType)
        {
            return contentType == ContentType.Blog;
        }

        public bool CanHaveChildrenOf(ContentType contentType)
        {
            return contentType == ContentType.Like ||
                contentType == ContentType.Comment;
        }
    }
}