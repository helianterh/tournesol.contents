﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Extension;

namespace Tournesol.Contents.Common.Model
{
    public class Content
    {
        public Guid? ContentId { get; set; }

        public ContentType ContentType { get; set; }

        public Guid? ParentId { get; set; }

        public string Json { get; set; }

        public string Api { get; set; }

        public VersionState State { get; set; }

        public Guid UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public Content? Parent { get; set; }

        public ICollection<Content>? Children { get; set; }

        public ICollection<ContentVersion>? Versions { get; set; }

        public Content(Guid? contentId, Guid? parentId, ContentType contentType, string json, string api, VersionState state, Guid updatedBy, DateTime updatedDate, Guid createdBy, DateTime createdDate)
        {
            ContentId = contentId;
            ParentId = parentId;
            ContentType = contentType;
            Json = json;
            Api = api;
            State = state;
            UpdatedBy = updatedBy;
            UpdatedDate = updatedDate;
            CreatedBy = createdBy;
            CreatedDate = createdDate;
        }
   
        public T GetContentObject<T>() where T : IContentObject
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(this.Json);
        }

        public static IContentObject CreateObject(ContentType contentType)
        {
            switch (contentType)
            {
                case ContentType.None: return null;
                case ContentType.Blog: return new Blog();
                case ContentType.BlogPost: return new BlogPost();
                case ContentType.Comment: return new Comment();
                case ContentType.Like: return new Like();
                case ContentType.Diary: return null;
                case ContentType.DiaryEntry: return null;
            }

            return null;
        }
    }
}
