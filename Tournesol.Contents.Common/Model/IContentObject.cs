﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Contents.Common.Model
{
    public interface IContentObject
    {
        public bool CanHaveChildrenOf(ContentType contentType);

        public bool CanBeChildOf(ContentType contentType);


    }
}
