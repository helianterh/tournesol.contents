﻿using System.ComponentModel.DataAnnotations.Schema;
using Tournesol.Contents.Common.Interface;

namespace Tournesol.Contents.Common.Model
{
    public class Comment : IContentObject
    {
        public string Text { get; set; }


        public bool CanBeChildOf(ContentType contentType)
        {
            return contentType == ContentType.Blog ||
                contentType == ContentType.BlogPost ||
                contentType == ContentType.Comment;
        }

        public bool CanHaveChildrenOf(ContentType contentType)
        {
            return contentType == ContentType.Like ||
                contentType == ContentType.Comment;
        }
    }
}