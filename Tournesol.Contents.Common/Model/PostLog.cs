﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Blug.Common.Model
{
    public class PostLog : Log
    {
        public Guid? PostId { get; set; }

        public PostState State { get; set; } 

        public Post? Post { get; set; }

        public PostLog(Guid? logId, Guid logBy, DateTime logDate, Guid? postId, PostState state) : base(logId, logBy, logDate)
        {
            PostId = postId;
            State = state;
        }
    }
}
