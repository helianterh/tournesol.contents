﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Blug.Common.Model
{
    public class CommentLog : Log
    {
        public Guid CommentId { get; set; }

        public CommentState State { get; set; } 

        public Comment? Comment { get; set; }

        public CommentLog(Guid? logId, Guid logBy, DateTime logDate, Guid commentId, CommentState state) : base(logId, logBy, logDate)
        {
            CommentId = commentId;
            State = state;
        }
    }
}
