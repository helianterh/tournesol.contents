﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Blug.Common.Model
{
    public class BlogLog : Blog, ILog
    {
        public Guid? LogId { get; set; }
        public Guid LogBy { get; set; }
        public DateTime LogDate { get; set; }

        public Blog? Blog { get; set; }

        public BlogLog(Guid? blogId, BlogState state, Guid? logId, Guid logBy, DateTime logDate) : base(blogId, state)
        {
            this.LogId = logId;
            this.LogBy = LogBy;
            this.LogDate = LogDate;
        }
    }
}
