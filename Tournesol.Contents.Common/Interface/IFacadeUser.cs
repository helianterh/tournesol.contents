﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Contents.Common.Interface
{
    public interface IFacadeUser
    {
        Guid GetCurrentUserGuid();
    }
}
