import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  _http: HttpClient;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this._http = http;

    /*
    http.get<Blog[]>(baseUrl + 'weatherforecast').subscribe(result => {
      this.forecasts = result;
    }, error => console.error(error));*/
  }

  ngOnInit(): void {
  }

  addBlog(): void {
    this._http.get<any>('https://localhost:44373/api/v1/AddBlog?title=x2&description=y2').subscribe(result =>
    {

    }, error => console.error(error));
  }

}

interface Blog {

}
