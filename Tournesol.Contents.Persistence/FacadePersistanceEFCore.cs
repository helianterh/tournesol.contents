﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Contents.Common.Interface;
using Tournesol.Contents.Common.Model;

namespace Tournesol.Contents.Persistence
{
    public class FacadePersistanceEFCore : IFacadePersistance
    {
        private static bool _firstTime = true;
        private static object _firstTimeLock = new object();

        private Action<DbContextOptionsBuilder> _actionBuilder;
        private Action<DbContext, bool> _actionContext;

        public FacadePersistanceEFCore(Action<DbContextOptionsBuilder> actionBuilder, Action<DbContext, bool> actionContext)
        {
            _actionBuilder = actionBuilder;
            _actionContext = actionContext;
        }

        public void UpdateContent(Content content)
        {
            using (var context = GetContext())
            {
                context.Content.Update(content);
                context.SaveChanges();
            }
        }

        public Content? GetContent(Guid contentId)
        {
            using (var context = GetContext())
            {
                return context.Content
                    .Include(e => e.Parent)
                    .Include(e => e.Children)
                    .Include(e => e.Versions)
                    .FirstOrDefault(e => e.ContentId == contentId);
            }
        }

        public IEnumerable<Content> GetContents(ContentType contentType)
        {
            using (var context = GetContext())
            {
                return context.Content
                    .Include(e => e.Parent)
                    .Include(e => e.Children)
                    .Include(e => e.Versions)
                    .Where(e => e.ContentType == contentType).ToList();
            }
        }

        private ContentDbContext GetContext()
        {
            var context = new ContentDbContext(_actionBuilder);

            lock (_firstTimeLock)
            {
                _actionContext!(context, _firstTime);
                _firstTime = false;
            }

            return context;
        }


    }
}
