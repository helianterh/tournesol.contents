﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Contents.Common.Model;
using Tournesol.Extension;

namespace Tournesol.Contents.Persistence.Configuration
{
    public class ContentVersionConfiguration : IEntityTypeConfiguration<ContentVersion>
    {
        public void Configure(EntityTypeBuilder<ContentVersion> builder)
        {
            int order = 0;

            builder.ToTable("CONTENT_VERSION");
            builder.HasKey(e => e.VersionId );

            builder.Property(e => e.ContentId)
                .HasColumnName("CONTENT_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.VersionId)
                .HasColumnName("VERSION_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.Json)
                .HasColumnName("JSON")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.Api)
                .HasColumnName("API")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.State)
                .HasColumnName("VERSION_STATE")
                .HasColumnOrder(order++)
                .HasColumnType("varchar")
                .HasMaxLength(3)
                .HasConversion(app_val => app_val.KeyValue(), bd_val => Enumerations.Get<VersionState>(bd_val))
                .IsRequired();

            builder.Property(e => e.UpdatedBy)
                .HasColumnName("UPDATED_BY")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.UpdatedDate)
                .HasColumnName("UPDATED_DATE")
                .HasColumnOrder(order++)
                .HasColumnType("date")
                .IsRequired()
                .HasConversion(app_val => app_val, bd_val => new DateTime(bd_val.Ticks, DateTimeKind.Utc));
        }
    }
}
