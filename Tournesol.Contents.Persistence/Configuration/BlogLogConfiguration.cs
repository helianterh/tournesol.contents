﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Blug.Common.Model;
using Tournesol.Extension;

namespace Tournesol.Blug.Persistence.Configuration
{
    public class BlogLogConfiguration : IEntityTypeConfiguration<BlogLog>
    {
        public void Configure(EntityTypeBuilder<BlogLog> builder)
        {
            int order = 0;

            builder.ToTable("BLOG_LOG");
            builder.HasKey(e => e.LogId);

            builder.Property(e => e.LogId)
                .HasColumnName("LOG_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.LogBy)
                .HasColumnName("LOG_BY")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.LogDate)
                .HasColumnName("LOG_DATE")
                .HasColumnOrder(order++)
                .IsRequired()
                .HasConversion<DateTime>(app_val => app_val, bd_val => new DateTime(bd_val.Ticks, DateTimeKind.Utc));

            builder.Property(e => e.BlogId)
                .HasColumnName("BLOG_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.State)
                .HasColumnName("BLOG_STATE")
                .HasColumnOrder(order++)
                .IsRequired() 
                .HasConversion<string>(app_val => app_val.KeyValue(), bd_val => Enumerations.Get<BlogState>(bd_val));
        }
    }
}
